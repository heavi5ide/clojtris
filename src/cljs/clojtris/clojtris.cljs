(ns clojtris
  (:require [clojure.browser.repl :as repl]
            [goog.dom :as dom]
            [goog.dom.classes :as classes]
            [goog.events :as events]
            goog.events.KeyHandler))

;; TETRIS MUTHAFUCKA!!!!!

;; The pieces (aka "tetrominoes" apparently)
(def tetrominoes [
  {:name "J"
   :shape [[1 0 0]
           [1 1 1]
           [0 0 0]]
   :position {:x 3
              :y 1}}
  {:name "L"
   :shape [[0 0 2]
           [2 2 2]
           [0 0 0]]
   :position {:x 3
              :y 1}}
  {:name "O"
   :shape [[3 3]
           [3 3]]
   :position {:x 4
              :y 1}}
  {:name "S"
   :shape [[0 4 4]
           [4 4 0]
           [0 0 0]]
   :position {:x 3
              :y 1}}
  {:name "T"
   :shape [[0 5 0]
           [5 5 5]
           [0 0 0]]
   :position {:x 3
              :y 1}}
  {:name "Z"
   :shape [[6 6 0]
           [0 6 6]
           [0 0 0]]
   :position {:x 3
              :y 1}}
  {:name "I"
   :shape [[0 0 0 0]
           [7 7 7 7]
           [0 0 0 0]
           [0 0 0 0]]
   :position {:x 3
              :y 0}}])

(def board-width 10)
(def board-height 19) ;; only 18 rows visible (top row is there to allow an I
                      ;; tetromino to rotate when first spawned, but is not
                      ;; drawn)

;; An infinite sequence of "random" tetrominos. Follows the "Tetris Guideline"
;; by generating each of the 7 pieces in some random order (a "bag" of pieces),
;; and then repeating the process with a new bag, ad infinitum.
;; See http://tetris.wikia.com/wiki/Random_Generator
;; The sequence is stored in an atom since it is stateful now -- if we don't
;; keep track of where we are in the sequence, we can't correctly generate our
;; 7-piece "bags" over time.
(def tetromino-generator (atom
                          (map tetrominoes
                               (flatten (repeatedly #(shuffle (range 7)))))))
(defn next-tetromino!
  "Pop the next tetromino from the infinite sequence"
  []
  (let [next-item (first @tetromino-generator)]
    (swap! tetromino-generator rest)
    next-item))

(defn- idx-2d
  "Index into a 2-dimensional vector"
  [data x y]
  ((data y) x))

(defn- rotate-row-right
  [shape size rownum]
  (vec
    (map
      (fn [i] ((shape i) rownum))
      (range (- size 1) -1 -1))))

(defn- rotate-row-left
  [shape size rownum]
  (vec
    (map
      (fn [i] ((shape i) (- size rownum 1)))
      (range size))))

(defn- rotate
  [shape rotate-row-fn]
  (let [size (count shape)
        rotate-row (partial rotate-row-fn shape size)]
    (vec (map rotate-row (range size)))))


(defn rotate-right
  "Rotate a shape matrix clockwise"
  [shape]
  (rotate shape rotate-row-right))

(defn rotate-left
  "Rotate a shape matrix counter-clockwise"
  [shape]
  (rotate shape rotate-row-left))

(defn valid-shape-position?
  "Test whether a shape matrix at a given x, y coordinate on a board is valid.
  Specially, checks whether any squares of the shape are out of the bounds of
  the game board, and whether any of the squares collide with existing squares
  on the board"
  [board shape position]
  (let [{pos-x :x pos-y :y} position
        size (count shape)
        max-x (- (count (board 0)) 1)
        max-y (- (count board) 1)
        in-bounds? (fn [x y]
                     (and (<= 0 x max-x)
                          (<= 0 y max-y)))
        open-position? (fn [x y]
                         (and (in-bounds? x y)
                              (= 0 (idx-2d board x y))))]
    (every? 
     (fn [y]
       (every? 
        (fn [x]
          (or (= 0 (idx-2d shape x y))
              (open-position? (+ pos-x x) (+ pos-y y))))
        (range size)))
     (range size))))

(defn line?
  "Tests a row for a line (all squares in the row are filled)"
  [row]
  (every? #(not= 0 %) row))

(defn count-completed-lines
  "Tests the game board for any completed lines"
  [board]
  (count (filter line? board)))

(defn score-value
  "Calculate the score of completed a given number of lines on a given level"
  [lines level]
  (cond (= 1 lines) (* 40 (+ level 1))
        (= 2 lines) (* 100 (+ level 1))
        (= 3 lines) (* 300 (+ level 1))
        (= 4 lines) (* 1200 (+ level 1))))

(defn empty-board
  "Create an empty (all zeroes) board of given width and height"
  [width height]
  (vec (replicate height (vec (replicate width 0)))))

(defn add-shape
  "Return a new board that has shape at position"
  [board shape position]
  (let [{pos-x :x pos-y :y} position
        size (count shape)]
    (vec (map-indexed (fn [y row]
                   (vec (map-indexed (fn [x board-value]
                                  (let [shape-x (- x pos-x)
                                        shape-y (- y pos-y)
                                        shape-val (if (and (< -1 shape-x size)
                                                           (< -1 shape-y size))
                                                    (idx-2d shape shape-x shape-y)
                                                    0)]
                                      (if (= 0 shape-val)
                                        board-value
                                        shape-val))) row))) board))))

(defn print-board
  "Prints a board structure to stdout"
  [board]
  (reduce (fn [accum row] (str accum (reduce str row) \newline)) "" board))

;; The game state atom
;; This is the only mutable state
(def current-game-state (atom {:shape []                  ;; The current
                                                          ;; falling shape
                               :position {:x 0
                                          :y 0}
                               :board (empty-board        ;; The game board
                                       board-width
                                       board-height)
                               :level 0
                               :lines 0
                               :score 0
                               :mode :pre-start           ;; Current game mode
                               :flashing false            ;; Whether we're
                               :flash-step 0              ;; flashing completed
                                                          ;; lines right now.
                               :tetromino-on-deck []      ;; The next piece
                               :clock nil}))              ;; js setTimeout id

(defn print-game-state
  "Prints a game state to stdout"
  [game-state]
  (let [{:keys [board shape position level lines score mode]} game-state]
    (str (print-board (add-shape board shape position))
    "LEVEL " level " -- LINES " lines " -- SCORE " score \newline
    (if (= mode :game-over) "GAME OVER" ""))))

(defn get-tick-interval
  "Return the milliseconds between ticks for a given level"
  [level]
  ;; 700 milliseconds for level 0. Reduce by 10 milliseconds per level, but
  ;; don't go below 100 milliseconds.
  (max 100 (- 1000 (* level 12))))

(declare tick)
(declare flash)
(defn setup-next-tick
  "Set a javascript timeout to tick the game forward if necessary"
  [game-state]
  (let [{:keys [clock level mode flashing]} game-state]
    (cond (= mode :game-over)
          (if (nil? clock)
            game-state
            (do
              (js/clearTimeout clock)
              (assoc game-state :clock nil)))
          (= mode :playing)
          (if flashing
            (do
              (js/clearTimeout clock)
              (assoc game-state
                :clock (js/setTimeout flash 80)))
            (do 
              (js/clearTimeout clock)
              (assoc game-state
                :clock (js/setTimeout tick (get-tick-interval level))))))))

(defn process-start
  "Start a new game"
  [game-state]
  (let [tetromino (next-tetromino!)]
    (assoc game-state
      :shape (tetromino :shape)
      :position (tetromino :position)
      :board (empty-board board-width board-height)
      :level 0
      :lines 0
      :score 0
      :mode :playing
      :flashing false
      :flash-step 0
      :tetromino-on-deck (next-tetromino!))))

(defn check-game-over
  "If the current tetromino overlaps an already-occupied square, game is over"
  [game-state]
  (let [{:keys [board shape position]} game-state]
    (if (valid-shape-position? board shape position)
      game-state
      (assoc game-state :mode :game-over))))

(defn process-done-falling
  "Lock current tetromino in place. If there are completed lines, transition
  to flashing state to flash the completed lines. Otherwise, create a new
  random tetromino at the top of the board and check for game-over condition"
  [game-state]
  (let [{:keys [board shape position mode score lines level tetromino-on-deck]} game-state
        next-board (add-shape board shape position)
        next-shape (tetromino-on-deck :shape)
        next-position (tetromino-on-deck :position)
        completed-lines (count-completed-lines next-board)
        next-lines (+ lines completed-lines)
        next-game-state (assoc game-state
                          :board next-board
                          :shape next-shape
                          :position next-position
                          :tetromino-on-deck (next-tetromino!))]
    (if (> completed-lines 0)
      (assoc next-game-state
        :flashing true
        :flash-step 0
        :score (+ score (score-value completed-lines level))
        :lines next-lines
        :level (quot next-lines 10))
      (check-game-over next-game-state))))

(defn process-tick
  "Process a tick event by moving the active tetromino down one square, or
  locking it into place on the board and generating a new current tetromino to
  start at the top of the board"
  [game-state]
  (let [{:keys [shape position board level]} game-state
        next-position (assoc position :y (+ (position :y) 1))]
    ;;(.log js/console (str "tick " (.now js/Date)))
    (if (valid-shape-position? board shape next-position)
      (assoc game-state :position next-position)
      (process-done-falling game-state))))

(defn remove-completed-lines
  [board]
  (let [board-without-lines (remove line? board)
        new-lines (- board-height (count board-without-lines))]
    (reduce conj (empty-board board-width new-lines) board-without-lines)))

(defn process-flash
  [game-state]
  (let [{:keys [flash-step board]} game-state]
    (if (< flash-step 8)
      (assoc game-state
        :flash-step (+ 1 flash-step))
      (assoc game-state
        :flashing false
        :flash-step 0
        :board (remove-completed-lines board)))))

(defn process-rotate-left
  "Process a rotate-left event by rotating the active tetromino
  counter-clockwise by 90 degress, or doing nothing if that rotation results in
  an invalid position"
  [game-state]
  (let [{:keys [shape position board]} game-state
        next-shape (rotate-left shape)]
    (if (valid-shape-position? board next-shape position)
      (assoc game-state :shape next-shape)
      game-state)))

(defn process-rotate-right
  "Process a rotate-right event by rotating the active tetromino clockwise by
  90 degress, or doing nothing if that rotation results in an invalid position"
  [game-state]
  (let [{:keys [shape position board]} game-state
        next-shape (rotate-right shape)]
    (if (valid-shape-position? board next-shape position)
      (assoc game-state :shape next-shape)
      game-state)))

(defn process-move-left
  "Process a move-left event by moving the active tetromino to the left one
  square, or doing nothing if that move results in an invalid position"
  [game-state]
  (let [{:keys [shape position board]} game-state
        next-position (assoc position :x (- (position :x) 1))]
    (if (valid-shape-position? board shape next-position)
      (assoc game-state :position next-position)
      game-state)))

(defn process-move-right
  "Process a move-right event by moving the active tetromino to the right one
  square, or doing nothing if that move results in an invalid position"
  [game-state]
  (let [{:keys [shape position board]} game-state
        next-position (assoc position :x (+ (position :x) 1))]
    (if (valid-shape-position? board shape next-position)
      (assoc game-state :position next-position)
      game-state)))
  
(defn next-game-state
  "Given a game state and an event, returns the state resulting from processing
  the event"
  [game-state event]
  (cond (= event :start) (setup-next-tick (process-start game-state))
        (= event :tick) (setup-next-tick (process-tick game-state))
        (= event :flash) (setup-next-tick (process-flash game-state))
        (= event :rotate-left) (process-rotate-left game-state)
        (= event :rotate-right) (process-rotate-right game-state)
        (= event :move-left) (process-move-left game-state)
        (= event :move-right) (process-move-right game-state)))

(defn update-text-display!
  "Update the text in the debug display <pre> element"
  [display-value]
  (dom/setTextContent (dom/getElement "board-display") display-value))

(defn update-score-display!
  "Updates the display of score, level, lines, etc"
  [game-state]
  (let [{:keys [level lines score mode]} game-state
        game-board-wrapper-el (dom/getElement "game-board-wrapper")]
    (dom/setTextContent (dom/getElement "score-display") (str score))
    (dom/setTextContent (dom/getElement "level-display") (str level))
    (dom/setTextContent (dom/getElement "lines-display") (str lines))
    (classes/enable game-board-wrapper-el "game-over" (= mode :game-over))
    (classes/enable game-board-wrapper-el "pre-start" (= mode :pre-start))))

(defn update-board-display!
  "Update the html of the board display div"
  [element-id display-value]
  (set! (.-innerHTML (dom/getElement element-id)) display-value))

(defn flash-board
  "Adjust the board to flash completed lines on or off appropriately"
  [board flash-step]
  (if (even? flash-step)
    board ;; On even numbered steps, show the entire board
    (vec (map                     ;; On odd numbered steps, show empty rows
          #(if (line? %)          ;; in place of completed lines
              (vec (replicate board-width 0))
              %) board))))

(defn render-dom-square
  [value]
  (if (= 0 value)
    (str "<span class=" \" "tile" \" "></span>")
    (str "<span class=" \" "tile tile-" value \" "></span>")))

(defn render-dom-row
  [row]
  (str "<div>" (reduce str (map render-dom-square row)) "</div>"))

(defn render-dom-board
  [display-board]
  (reduce str (map render-dom-row display-board)))

(defn render-dom-display
  "Render the current game state as a series of <div> rows with <span> squares"
  [game-state]
  (let [{:keys [board shape position mode flashing flash-step]} game-state
        display-board (cond flashing (flash-board board flash-step)
                            (= mode :pre-start) board
                            :else (add-shape board shape position))]
    (render-dom-board (drop 1 display-board))))

(defn update-all-displays!
  "Update all DOM elements to reflect the current state"
  [game-state]
  (let [next-shape ((game-state :tetromino-on-deck) :shape)]
    ;;(update-text-display! (print-game-state @current-game-state))
    (update-board-display! "game-board" (render-dom-display game-state))
    (update-board-display! "shape-on-deck" (render-dom-board next-shape))
    (update-score-display! @current-game-state)))

(defn update-game-state!
  "Mutates the current-game-state atom in response to an event to move the game
  forward. Also sets an appropriate (according to current-game-state)
  Javascript timeout that will cause the next tick event to fire if necessary."
  [event]
  (swap! current-game-state next-game-state event)
  (update-all-displays! @current-game-state))

(defn start-game [] (update-game-state! :start))
(defn tick [] (update-game-state! :tick))
(defn flash [] (update-game-state! :flash))

(defn handle-key-event
  "Handle keyboard events"
  [event]
  (let [key (.-keyCode event)
        repeat (.-repeat event)
        {:keys [flashing mode]} @current-game-state]
    (if flashing
      
      nil ;; Ignore any keyboard events while we're flashing completed lines
      
      (if (or (= mode :game-over) (= mode :pre-start))
        (cond (= key 83) (update-game-state! :start))       ;; s
        
        (cond (= key 37) (update-game-state! :move-left)    ;; left arrow
              (= key 39) (update-game-state! :move-right)   ;; right arrow
              (= key 40) (update-game-state! :tick)         ;; down arrow is same as
                                                           ;; a clock tick... i think?
              (or (= key 38)                                   ;; up arrow
                  (= key 90)) (update-game-state! :rotate-left) ;; z
              (= key 88) (update-game-state! :rotate-right)     ;; x
              )))))

(defn init
  "Initialize the key event listener that will drive everything else"
  []
  (let [key-handler (events/KeyHandler. js/document true)]
    (.addEventListener key-handler events/KeyHandler.EventType.KEY handle-key-event true)
    (update-all-displays! @current-game-state)))

;;(.addEventListener js/window "load" #(repl/connect "http://localhost:9000/repl"))
(.addEventListener js/window "load" init)